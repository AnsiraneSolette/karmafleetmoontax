Sample Input:

```
Cobaltite 526,789 Material 3,125.53 m3 139,249,155.51 ISK
Bitumens 688,453 50,404.50ISK
Sylvite 253,680
```

or

```
Compressed Cobaltite 526,789 Material 3,125.53 m3 139,249,155.51 ISK
Compressed Bitumens 688,453 50,404.50ISK
Compressed Sylvite 253,680
```

Output:

```
454,773,914
```

Sample Input (EU - Belgium localized (, and . switched) )

```
Zeolites    1.200    Ubiquitous Moon Asteroids    Asteroid            12.000 m3    1.202.112,00 ISK
```



Development:
```
cd app
yarn install
yarn start
```

Update Prices:
```
cd app
node update_prices.js
```