let itemIds = require('./src/item_ids.json')
let allItemIds = ''
Object.keys(itemIds).forEach(key => {
  let itemId = itemIds[key]
  allItemIds += itemId + ','
})
console.log(allItemIds)

const https = require('https')
var xml2js = require('xml2js')
const fs = require('fs')

function getKeyByValue (object, value) {
  return Object.keys(object).find(key => object[key] == value)
}

let newPrices = {}

https.get(
  'https://goonmetrics.apps.goonswarm.org/api/price_data/?station_id=1030049082711&type_id=' +
    allItemIds,
  resp => {
    let data = ''
    let parser = new xml2js.Parser()
    resp.on('data', chunk => {
      data += chunk
    })
    resp.on('end', () => {
      parser.parseString(data, function (err, result) {
        console.log(result.goonmetrics.price_data[0].type[0])
        let prices = result.goonmetrics.price_data[0].type
        prices.forEach(element => {
          let itemId = element['$'].id
          let price = element.buy[0].max[0]
          newPrices[getKeyByValue(itemIds, itemId)] = Number.parseFloat(price)
        })
      })

      // Update the prices.json file
      console.log(newPrices)
      fs.writeFile(
        'src/prices.json',
        JSON.stringify(newPrices, null, 4),
        err => {
          if (err) {
            throw err
          }
          console.log('JSON data is saved.')
        }
      )
      var date = new Date()
      var utcString = date.toUTCString()
      fs.writeFile(
        'src/prices_date.js',
        'const price_time = ' +
          JSON.stringify(utcString, null, 4) +
          '; export default price_time;',
        err => {
          if (err) {
            throw err
          }
          console.log('JSON data is saved.')
        }
      )
    })
  }
)
